<?php

    // Control core classes for avoid errors
if( class_exists( 'CSF' ) ) {

    //
    // Set a unique slug-like ID
    $prefix = 'chakaride-booking-meta';
  
    //
    // Create a metabox
    CSF::createMetabox( $prefix, array(
      'title'     => 'Booking Charge',
      'post_type' => 'product',
      'data_type'  => 'unserialize',
    ) );
  
    //
    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Daily Basis',
      'fields' => array(
  
        //
        // A text field
        array(
          'id'    => 'crmeta-inside-dhaka',
          'type'  => 'number',
          'title' => 'Inside Dhaka',
        ),

        array(
            'id'    => 'crmeta-outside-dhaka',
            'type'  => 'number',
            'title' => 'Outside Dhaka',
          ),
  
      )
    ) );
  
    //
    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Airport Booking',
      'fields' => array(
  
        // A textarea field
        array(
            'id'    => 'crmeta-airport-one-way',
            'type'  => 'number',
            'title' => 'One Way',
          ),
  
          array(
              'id'    => 'crmeta-airport-round-trip',
              'type'  => 'number',
              'title' => 'Round Trip',
            ),
  
      )
    ) );
  
  }
  