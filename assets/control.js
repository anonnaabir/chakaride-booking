// var all_data;

// Booking For Control After Click On Booking Type

jQuery(function(){
  jQuery('#inside-dhaka-booking,#outside-dhaka-booking,#airport-booking,#daily-basis-inside-dhaka,#daily-basis-outside-dhaka').click(function(){
    booking_type = this.id
    var expiretime = 0.5;
      Cookies.set('chakaride-booking-type', booking_type, { expires: expiretime });
    console.log(booking_type);
    window.location = "/shop";
  })
});



jQuery(function(){
  jQuery('#daily-basis-booking').click(function(){
    jQuery("#inside-dhaka-booking,#outside-dhaka-booking,#airport-booking,#daily-basis-booking").css("display", "none");
    jQuery("#daily-basis-inside-dhaka,#daily-basis-outside-dhaka,#booking-selector-back").css("display", "block");
  })
});

jQuery(function(){
  jQuery('#booking-selector-back').click(function(){
    jQuery("#daily-basis-inside-dhaka,#daily-basis-outside-dhaka,#booking-selector-back").css("display", "none");
    jQuery("#inside-dhaka-booking,#outside-dhaka-booking,#airport-booking,#daily-basis-booking").css("display", "block");
  })
});

// Trip Type Selector Function
jQuery(function(){
jQuery("#chakaride-trip-type").change(function(){
  tripvalue = jQuery(this).children("option:selected").val();
  triplabel = jQuery(this).children("option:selected").text();
  // console.log(tripvalue + triplabel);
  get_trip_type(tripvalue,triplabel);
});
});


// Pickup Date Picker Function

  jQuery(function(){
    jQuery("#chakaride-booking-date").change(function(){
      var crdate = new Date(jQuery('#chakaride-booking-date').val());
      // var datepicker = new Date(jQuery('#chakaride-booking-date').val());
      // var date = datepicker.getDate();
      // var month = datepicker.getMonth() + 1;
      // var year = datepicker.getFullYear();
      // crdate = date + "/" + month + "/" + year;
      console.log(crdate);
      get_booking_date(crdate);
    })
  })

  // Pickup Time Picker Function

  jQuery(function(){
    jQuery("#chakaride-pickup-time").change(function(){
      var timepicker = jQuery('#chakaride-pickup-time').val();
      console.log(timepicker);
      get_pickup_time(timepicker);
    })
  })



  // Dropoff Date Picker Function

  jQuery(function(){
    jQuery("#chakaride-dropoff-date").change(function(){
      var crdate = new Date(jQuery('#chakaride-dropoff-date').val());
      // var datepicker = new Date(jQuery('#chakaride-dropoff-date').val());
      // var date = datepicker.getDate();
      // var month = datepicker.getMonth() + 1;
      // var year = datepicker.getFullYear();
      // crdate = date + "/" + month + "/" + year;
      console.log(crdate);
      get_dropoff_date(crdate);
    })
  })


    // Dropoff Time Picker Function

    jQuery(function(){
      jQuery("#chakaride-dropoff-time").change(function(){
        var timepicker = jQuery('#chakaride-dropoff-time').val();
        console.log(timepicker);
        get_dropoff_time(timepicker);
      })
    })


function get_trip_type(tripvalue,triplabel) {
  tripType = tripvalue
  tripTypeText = triplabel
  var expiretime = 0.5;
  Cookies.set('chakaride-trip-type', tripType, { expires: expiretime });
  final_distence();
  console.log(tripType + tripTypeText);
}

function get_booking_date(crdate) {
  BookingDate = crdate;
  var expiretime = 0.5;
  Cookies.set('chakaride-booking-date', BookingDate, { expires: expiretime });
  final_distence();
  console.log(BookingDate);
}


function get_pickup_time(crtime) {
  pickupTime = crtime;
  var expiretime = 0.5;
  Cookies.set('chakaride-pickup-time', pickupTime, { expires: expiretime });
  final_distence();
  console.log(pickupTime);
}


function get_dropoff_date(crdate) {
  DropoffDate = crdate;
  var expiretime = 0.5;
  Cookies.set('chakaride-dropoff-date', DropoffDate, { expires: expiretime });
  const diffTime = Math.abs(DropoffDate - BookingDate);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  Cookies.set('chakaride-booking-duration', diffDays, { expires: expiretime });
  console.log(diffDays + " days");
  final_distence();
  console.log(DropoffDate);
}


function get_dropoff_time(crtime) {
  dropoffTime = crtime;
  var expiretime = 0.5;
  Cookies.set('chakaride-dropoff-time', dropoffTime, { expires: expiretime });
  final_distence();
  console.log(dropoffTime);
}

function load_pickup_location(){
  latest_data = [];
    var data = all_data;

            var output_data = data.map(function(val, index){
              // console.log(val);
              // latest_data.push(val.properties.label);
              latest_data.push(val);
              })

              console.log(latest_data);

              // var html;

  //  if (latest_data) {

      if (latest_data.length == 0) {
        console.log("No Location Found");
        var pickup_location_name = "<p>No Location Found. Please search with another location</p>";
        document.getElementById("cr_pickup_modal").innerHTML = pickup_location_name;
      }

      else {
        var pickup_location_name = "<select class='form-select' id='select_pickup'>";
        pickup_location_name += "<option disabled selected value> -- Select Available Pickup Location -- </option>";
        for (var i=0; i<latest_data.length; i++) {
          pickup_location_name +=
          "<option value='"+
          latest_data[i]['geometry']['coordinates'][0]+
          ","+
          latest_data[i]['geometry']['coordinates'][1]+
          "'>" +
          latest_data[i]['properties']['label']
          +"</option>";
        }
        pickup_location_name += "</select>";
        document.getElementById("cr_pickup_modal").innerHTML = pickup_location_name;

        final_pickup_location = document.getElementById('select_pickup');
        selected_pickup_location();
  }

  }


  function selected_pickup_location() {
    pickupLocation = "";
    pickupLocationText = "";
    // var calculate_output = jQuery( "#select_pickup" ).find(":selected").text();
    // console.log("Select Field Output: "+ calculate_output);
    jQuery("#select_pickup").change(function(){
      pickupLocation = jQuery(this).children("option:selected").val();
      pickupLocationText = jQuery(this).children("option:selected").text();
      pickup_map(pickupLocation);
      // console.log("You have selected the country - " + pickupLocationText);
  });

  // final_distence();

  }


  function load_dropoff_location(){
    latest_data = [];
      var data = all_data;

              var output_data = data.map(function(val, index){
                // console.log(val);
                // latest_data.push(val.properties.label);
                latest_data.push(val);
                })

                // console.log(latest_data);

                // var html;

    //  if (latest_data) {

      if (latest_data.length == 0) {
        console.log("No Location Found");
        var dropoff_location_name = "<p>No Location Found. Please search with another location</p>";
        document.getElementById("cr_dropoff_modal").innerHTML = dropoff_location_name;
      }

      else {
        var dropoff_location_name = "<select class='form-select' id='select_dropoff'>";
        dropoff_location_name += "<option disabled selected value> -- Select Available Dropoff Location -- </option>";
        for (var i=0; i<latest_data.length; i++) {
          dropoff_location_name +=
          "<option value='"+
          latest_data[i]['geometry']['coordinates'][0]+
          ","+
          latest_data[i]['geometry']['coordinates'][1]+
          "'>" +
          latest_data[i]['properties']['label']
          +"</option>";
        }
      dropoff_location_name += "</select>";
      document.getElementById("cr_dropoff_modal").innerHTML = dropoff_location_name;

      final_dropoff_location = document.getElementById('select_dropoff');

      selected_dropoff_location();

      }

    }





    function selected_dropoff_location() {
      dropoffLocation = "";
      dropoffLocationText = "";

    jQuery("#select_dropoff").change(function(){
      dropoffLocation = jQuery(this).children("option:selected").val();
      dropoffLocationText = jQuery(this).children("option:selected").text();
      dropoff_map(dropoffLocation);
  });

    }


    function final_distence() {

      if (typeof dropoffLocation == 'undefined') {
        jQuery(function(){
        var expiretime = 0.5;
        Cookies.set('chakaride-pickup-location', pickupLocationText, { expires: expiretime })
        var selected_pickup_location = Cookies.get('chakaride-pickup-location');
        jQuery("#cr_pickup").val(selected_pickup_location);
        console.log("Your Cookie Pickup Location is "+ selected_pickup_location);
        });
      }

      else if (typeof BookingDate == 'undefined') {
        jQuery(function(){
          var expiretime = 0.5;
          Cookies.set('chakaride-dropoff-location', dropoffLocationText, { expires: expiretime })
          var selected_dropoff_location = Cookies.get('chakaride-dropoff-location');
          jQuery("#cr_dropoff").val(selected_dropoff_location);
          console.log("Your Dropoff Location is "+ selected_dropoff_location);
        });
      }


      else {
        // if (BookingDate && tripType && pickupTime != 'undefined') {

        get_distance(pickupLocation,dropoffLocation);
          total_distance = final_distance;
          var expiretime = 0.5;
          Cookies.set('chakaride-total-distance', total_distance, { expires: expiretime })

        // if (tripType == "round-trip") {
        //   total_distance = final_distance*2;
        //   var expiretime = 0.5;
        //   Cookies.set('chakaride-total-distance', total_distance, { expires: expiretime })
        // }
        // else {
        //   total_distance = final_distance;
        //   var expiretime = 0.5;
        //   Cookies.set('chakaride-total-distance', total_distance, { expires: expiretime })
        // }

        console.log("Total Distance " + total_distance);

      }

    }
