    function pickup_map(pickupLocation) {
        // pickupLocation.map(function(val as pickupLocation){
        //     console.log(val);
        // })

        var latlng = pickupLocation.split(',');
        var pickup_latitude = parseFloat(latlng[0]);
        var pickup_longitude = parseFloat(latlng[1]);
        // console.log(pickup_latitude);
        // console.log(typeof pickup_latitude);
        mapboxgl.accessToken = 'pk.eyJ1IjoiYW5vbm5hYWJpciIsImEiOiJja2t6NmdiY3YwMjY3MnhwamhiYWt6a2plIn0.LpMmOT9H95m5mKsh1wMd5Q';
        var map = new mapboxgl.Map({
        container: 'pickup_map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [pickup_latitude,pickup_longitude],
        zoom: 14
        });
 
        var marker = new mapboxgl.Marker()
        .setLngLat([pickup_latitude,pickup_longitude],)
        .addTo(map);
    }


    function dropoff_map(dropoffLocation) {

        var latlng = dropoffLocation.split(',');
        var dropoff_latitude = parseFloat(latlng[0]);
        var dropoff_longitude = parseFloat(latlng[1]);
        // console.log(pickup_latitude);
        // console.log(typeof pickup_latitude);
        mapboxgl.accessToken = 'pk.eyJ1IjoiYW5vbm5hYWJpciIsImEiOiJja2t6NmdiY3YwMjY3MnhwamhiYWt6a2plIn0.LpMmOT9H95m5mKsh1wMd5Q';
        var map = new mapboxgl.Map({
        container: 'dropoff_map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [dropoff_latitude,dropoff_longitude],
        zoom: 14
        });
 
        var marker = new mapboxgl.Marker()
        .setLngLat([dropoff_latitude,dropoff_longitude],)
        .addTo(map);

    }

