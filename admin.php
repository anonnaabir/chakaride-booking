<?php

    // Control core classes for avoid errors
    if( class_exists( 'CSF' ) ) {

    //
    // Set a unique slug-like ID
    $prefix = 'chakaride_booking';
    
  
    //
    // Create options
    CSF::createOptions( $prefix, array(
      'framework_title'  => 'Chakaride Booking',
      'menu_title' => 'Chakaride Booking',
      'menu_slug'  => 'chakaride-booking',
    ) );
  
    //

    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Daily Fuel Cost',
      'fields' => array(
  
        //
        // A text field
        array(
            'id'    => 'cr-inside-dhaka-fuel-cost',
            'type'  => 'number',
            'title' => 'Inside Dhaka (BDT/KM)',
          ),

          array(
            'id'    => 'cr-outside-dhaka-fuel-cost',
            'type'  => 'number',
            'title' => 'Outside Dhaka (BDT/KM)',
          ),
          
  
      )
    ) );
  
    //
    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Airport Fuel Cost',
      'fields' => array(
  
        array(
            'id'    => 'cr-airport-oneway-fuel-cost',
            'type'  => 'number',
            'title' => 'One Way (BDT/KM)',
          ),

          array(
            'id'    => 'cr-airport-round-fuel-cost',
            'type'  => 'number',
            'title' => 'Round Trip (BDT/KM)',
          ),
  
        )
    ));
  
  }
  