<?php

    // Control core classes for avoid errors
if( class_exists( 'CSF' ) ) {

    //
    // Set a unique slug-like ID
    $prefix = 'chakaride-fuel-meta';
    $fuel_meta = 'chakaride-fuel-meta-outside';

    //
    // Create a metabox
    CSF::createMetabox( $prefix, array(
      'title'     => 'Fuel Cost',
      'post_type' => 'product',
      'data_type'  => 'unserialize',
    ) );

    //
    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Inside Dhaka',
      'fields' => array(

        //
        // A text field
        array(
          'id'    => 'crfuel-inside-dhaka',
          'type'  => 'number',
          'title' => 'Cost Per KM',
        ),

      )
    ) );

    //
    // Create a section
    CSF::createSection( $prefix, array(
      'title'  => 'Outside Dhaka',
      'fields' => array(

        // A textarea field
        // array(
        //   'id'    => 'crfuel-outside-dhaka',
        //   'type'  => 'number',
        //   'title' => 'Cost Per KM',
        // ),

      )
    ) );


    // Outside Dhaka Fuel Cost
    CSF::createMetabox( $fuel_meta, array(
      'title'     => 'Fuel Cost (Outside Dhaka)',
      'post_type' => 'product',
      'data_type'  => 'unserialize',
    ) );


    // One way Fuel Cost
    CSF::createSection( $fuel_meta, array(
      'title'  => 'One Way',
      'fields' => array(

        array(
          'id'    => 'crfuel-outside-dhaka-one',
          'type'  => 'number',
          'title' => 'Cost Per KM',
        ),

      )
    ) );


    // One way Fuel Cost
    CSF::createSection( $fuel_meta, array(
      'title'  => 'Round Trip',
      'fields' => array(

        array(
          'id'    => 'crfuel-outside-dhaka-round',
          'type'  => 'number',
          'title' => 'Cost Per KM',
        ),

      )
    ) );

  }
