<?php


    function chakaride_assets() {
        if (is_product() OR is_page('booking')) {
        wp_register_style( 'style',    plugins_url( 'assets/style.css',    __FILE__ ), true,'1.0');
        wp_register_style( 'bootstrap',    plugins_url( 'assets/bootstrap.min.css',    __FILE__ ), true,'1.0');
        wp_register_style( 'timepicker',    plugins_url( 'assets/jquery.timepicker.css',    __FILE__ ), true,'1.0');
        wp_enqueue_style ( 'style' );
        wp_enqueue_style ( 'bootstrap' );
        wp_enqueue_style ( 'timepicker' );
        wp_enqueue_style( 'mapboxcss', 'https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' );
        wp_enqueue_style( 'jquerycss', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
        }
        wp_enqueue_script( 'cookie', plugin_dir_url( __FILE__ ) . 'cookie.js', array('jquery'), true, '1.0' );
        wp_enqueue_script( 'main', plugins_url( 'assets/main.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'map', plugins_url( 'assets/map.js', __FILE__ ), array(), '1.0');
        wp_enqueue_script( 'mapview', plugins_url( 'assets/mapview.js', __FILE__ ), array(), '1.0');
        wp_enqueue_script( 'control', plugins_url( 'assets/control.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'distance', plugins_url( 'assets/distance.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'bsjs', plugins_url( 'assets/bootstrap.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'timepicker', plugins_url( 'assets/jquery.timepicker.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'mapbox', 'https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js', array(), null, true );
        wp_enqueue_script( 'jqueryui', plugins_url( 'assets/jqueryui.js', __FILE__ ), array('jquery'), '1.0');
    }

    add_action('wp_enqueue_scripts', 'chakaride_assets');




    // Include Codestar Framework

    require_once plugin_dir_path( __FILE__ ) .'/inc/csf/codestar-framework.php';
    // require_once plugin_dir_path( __FILE__ ) .'admin.php';
    require_once plugin_dir_path( __FILE__ ) .'booking-meta.php';
    require_once plugin_dir_path( __FILE__ ) .'fuel-meta.php';
    // require_once plugin_dir_path( __FILE__ ) .'user-meta.php';



    // Calculate Total Price (Base Fair + Booking Charge + Distance Fair )

      function chakaride_calculate_price($cart_object) {

        $chakaride_options = get_option( 'chakaride_booking' );

        $get_booking_type = $_COOKIE["chakaride-booking-type"];
        $get_trip_type = $_COOKIE["chakaride-trip-type"];
        $get_distance = intval($_COOKIE["chakaride-total-distance"])+1;
        $get_booking_duration = intval($_COOKIE["chakaride-booking-duration"]);

          foreach ( $cart_object->get_cart() as $hash => $value ) {

            // Get Product ID

            $product_id = $value['data']->id;

            // Get Product Base Brice

            $base_price = $value['data']->get_price();



            // Condition Work For Inside Dhaka

            if ($get_booking_type == 'inside-dhaka-booking') {
              $inside_dhaka_fuel_cost = get_post_meta( $product_id, 'crfuel-inside-dhaka', true );
              $distance_cost = 80*$inside_dhaka_fuel_cost;
              $final_price = intval($base_price) + $distance_cost;
            }



            // Condition Work For Outside Dhaka

            if ($get_booking_type == 'outside-dhaka-booking') {

              if ($get_trip_type == 'one-way') {
                $outside_dhaka_fuel_cost = get_post_meta( $product_id, 'crfuel-outside-dhaka-one', true );
              }

              elseif ($get_trip_type == 'round-trip') {
                $outside_dhaka_fuel_cost = get_post_meta( $product_id, 'crfuel-outside-dhaka-round', true );
              }

              $distance_cost = $get_distance*$outside_dhaka_fuel_cost;
              $final_price = intval($base_price) + $distance_cost;
            }


            // Condition Start For Airport Booking

            if ($get_booking_type == 'airport-booking') {

              if ($get_trip_type == 'one-way') {
                $airport_charge = get_post_meta( $product_id, 'crmeta-airport-one-way', true );
              }

              if ($get_trip_type == 'round-trip') {
                $airport_charge = get_post_meta( $product_id, 'crmeta-airport-round-trip', true );
              }

              $final_price = intval($base_price)*$airport_charge/100;
            }


            // Condition Start For Daily Basic Inside Dhaka

            if ($get_booking_type == 'daily-basis-inside-dhaka') {
              $inside_dhaka_booking_charge = get_post_meta( $product_id, 'crmeta-inside-dhaka', true );
              $total_base_price = intval($base_price) + intval($base_price)*$inside_dhaka_booking_charge/100;

              if ($get_booking_duration > 0) {
                $final_price =  $total_base_price*($get_booking_duration+1);
              }

              else {
                $final_price = $total_base_price;
              }

            }



            // Condition Start For Daily Basic Outside Dhaka

            if ($get_booking_type == 'daily-basis-outside-dhaka') {
              $outside_dhaka_booking_charge = get_post_meta( $product_id, 'crmeta-outside-dhaka', true );
              $total_base_price = intval($base_price) + intval($base_price)*$outside_dhaka_booking_charge/100;
              if ($get_booking_duration > 0) {
                $final_price =  $total_base_price*($get_booking_duration+1);

              }

              else {
                $final_price = $total_base_price;
              }

            }


            // Final Price

            $value['data']->set_price($final_price);
          }

      }

      add_action( 'woocommerce_before_calculate_totals', 'chakaride_calculate_price' );




      // Display All Informations in Cart

      function display_custom_item_data($cart_item_data,$cart_item) {

          $get_booking_type = $_COOKIE["chakaride-booking-type"];
          $get_pickup_location = $_COOKIE["chakaride-pickup-location"];
          $get_dropoff_location = $_COOKIE["chakaride-dropoff-location"];
          $get_distance = $_COOKIE["chakaride-total-distance"];
          $get_trip_type = $_COOKIE["chakaride-trip-type"];
          $get_pickup_date = $_COOKIE["chakaride-booking-date"];
          $get_pickup_time = $_COOKIE["chakaride-pickup-time"];
          $get_dropoff_date = $_COOKIE["chakaride-dropoff-date"];
          $get_dropoff_time = $_COOKIE["chakaride-dropoff-time"];

          if ($get_booking_type == 'inside-dhaka-booking') {
            $cart_item_data[] = array(
              'name' => __("Booking Type", "chakaride-booking" ),
              'value' =>  $get_booking_type,
          );

            $cart_item_data[] = array(
                'name' => __("Pickup Location", "chakaride-booking" ),
                'value' =>  $get_pickup_location,
            );

            $cart_item_data[] = array(
              'name' => __("Pickup Date", "chakaride-booking" ),
              'value' =>  $get_pickup_date,
          );

            $cart_item_data[] = array(
              'name' => __("Pickup Time", "chakaride-booking" ),
              'value' =>  $get_pickup_time,
          );

        //   $cart_item_data[] = array(
        //     'name' => __("Distance", "chakaride-booking" ),
        //     'value' =>  $get_distance,
        // );

          }


          if ($get_booking_type == 'outside-dhaka-booking') {

                $cart_item_data[] = array(
                'name' => __("Booking Type", "chakaride-booking" ),
                'value' =>  $get_booking_type,
            );

              $cart_item_data[] = array(
                  'name' => __("Pickup Location", "chakaride-booking" ),
                  'value' =>  $get_pickup_location,
              );

              $cart_item_data[] = array(
                  'name' => __("Dropoff Location", "chakaride-booking" ),
                  'value' =>  $get_dropoff_location,
              );


              $cart_item_data[] = array(
                'name' => __("Trip Type", "chakaride-booking" ),
                'value' =>  $get_trip_type,
            );


              $cart_item_data[] = array(
                'name' => __("Pickup Date", "chakaride-booking" ),
                'value' =>  $get_pickup_date,
            );

              $cart_item_data[] = array(
                'name' => __("Pickup Time", "chakaride-booking" ),
                'value' =>  $get_pickup_time,
            );

          }


          if ($get_booking_type == 'airport-booking') {

               $cart_item_data[] = array(
                'name' => __("Booking Type", "chakaride-booking" ),
                'value' =>  $get_booking_type,
            );

              $cart_item_data[] = array(
                  'name' => __("Pickup Location", "chakaride-booking" ),
                  'value' =>  $get_pickup_location,
              );

              $cart_item_data[] = array(
                  'name' => __("Dropoff Location", "chakaride-booking" ),
                  'value' =>  $get_dropoff_location,
              );


              $cart_item_data[] = array(
                'name' => __("Trip Type", "chakaride-booking" ),
                'value' =>  $get_trip_type,
            );


              $cart_item_data[] = array(
                'name' => __("Pickup Date", "chakaride-booking" ),
                'value' =>  $get_pickup_date,
            );

              $cart_item_data[] = array(
                'name' => __("Pickup Time", "chakaride-booking" ),
                'value' =>  $get_pickup_time,
            );

      }


            if ($get_booking_type == 'daily-basis-inside-dhaka') {

              $cart_item_data[] = array(
                'name' => __("Booking Type", "chakaride-booking" ),
                'value' =>  $get_booking_type,
            );

              $cart_item_data[] = array(
                  'name' => __("Pickup Location", "chakaride-booking" ),
                  'value' =>  $get_pickup_location,
              );


              $cart_item_data[] = array(
                'name' => __("Pickup Date", "chakaride-booking" ),
                'value' =>  $get_pickup_date,
            );

              $cart_item_data[] = array(
                'name' => __("Pickup Time", "chakaride-booking" ),
                'value' =>  $get_pickup_time,
            );

            $cart_item_data[] = array(
              'name' => __("Finish Date", "chakaride-booking" ),
              'value' =>  $get_dropoff_date,
          );

        }


        if ($get_booking_type == 'daily-basis-outside-dhaka') {
              $cart_item_data[] = array(
                'name' => __("Booking Type", "chakaride-booking" ),
                'value' =>  $get_booking_type,
            );

              $cart_item_data[] = array(
                  'name' => __("Pickup Location", "chakaride-booking" ),
                  'value' =>  $get_pickup_location,
              );


              $cart_item_data[] = array(
                  'name' => __("Dropoff Location", "chakaride-booking" ),
                  'value' =>  $get_dropoff_location,
              );


              $cart_item_data[] = array(
                'name' => __("Pickup Date", "chakaride-booking" ),
                'value' =>  $get_pickup_date,
            );

              $cart_item_data[] = array(
                'name' => __("Pickup Time", "chakaride-booking" ),
                'value' =>  $get_pickup_time,
            );

            $cart_item_data[] = array(
              'name' => __("Finish Date", "chakaride-booking" ),
              'value' =>  $get_dropoff_date,
          );

        }

          return $cart_item_data;
      }

      add_filter( 'woocommerce_get_item_data', 'display_custom_item_data', 10, 2 );



      // Create Order Items and Lines

    function chakaride_create_order_items_meta($item,$cart_item_key,$values,$order) {

        $get_booking_type = $_COOKIE["chakaride-booking-type"];
        $get_pickup_location = $_COOKIE["chakaride-pickup-location"];
        $get_dropoff_location = $_COOKIE["chakaride-dropoff-location"];
        $get_distance = $_COOKIE["chakaride-total-distance"];
        $get_trip_type = $_COOKIE["chakaride-trip-type"];
        $get_pickup_date = $_COOKIE["chakaride-booking-date"];
        $get_pickup_time = $_COOKIE["chakaride-pickup-time"];
        $get_dropoff_date = $_COOKIE["chakaride-dropoff-date"];
        $get_dropoff_time = $_COOKIE["chakaride-dropoff-time"];

        if ($get_booking_type == 'inside-dhaka-booking') {
          $item->add_meta_data( __( 'Booking Type', 'chakaride-booking' ), $get_booking_type );
          $item->add_meta_data( __( 'Pickup Location', 'chakaride-booking' ), $get_pickup_location );
          $item->add_meta_data( __( 'Total Distance', 'chakaride-booking' ), $get_distance." KM" );
          $item->add_meta_data( __( 'Pickup Date', 'chakaride-booking' ), $get_pickup_date );
          $item->add_meta_data( __( 'Pickup Time', 'chakaride-booking' ), $get_pickup_time);
      }

        if ($get_booking_type == 'outside-dhaka-booking') {
        $item->add_meta_data( __( 'Booking Type', 'chakaride-booking' ), $get_booking_type );
        $item->add_meta_data( __( 'Pickup Location', 'chakaride-booking' ), $get_pickup_location );
        $item->add_meta_data( __( 'Dropoff Location', 'chakaride-booking' ), $get_dropoff_location );
        $item->add_meta_data( __( 'Total Distance', 'chakaride-booking' ), $get_distance." KM" );
        $item->add_meta_data( __( 'Trip Type', 'chakaride-booking' ), $get_trip_type);
        $item->add_meta_data( __( 'Pickup Date', 'chakaride-booking' ), $get_pickup_date );
        $item->add_meta_data( __( 'Pickup Time', 'chakaride-booking' ), $get_pickup_time);
      }


        if ($get_booking_type == 'airport-booking') {
        $item->add_meta_data( __( 'Booking Type', 'chakaride-booking' ), $get_booking_type );
        $item->add_meta_data( __( 'Pickup Location', 'chakaride-booking' ), $get_pickup_location );
        $item->add_meta_data( __( 'Dropoff Location', 'chakaride-booking' ), $get_dropoff_location );
        $item->add_meta_data( __( 'Total Distance', 'chakaride-booking' ), $get_distance." KM" );
        $item->add_meta_data( __( 'Trip Type', 'chakaride-booking' ), $get_trip_type);
        $item->add_meta_data( __( 'Pickup Date', 'chakaride-booking' ), $get_pickup_date );
        $item->add_meta_data( __( 'Pickup Time', 'chakaride-booking' ), $get_pickup_time);
      }


      if ($get_booking_type == 'daily-basis-inside-dhaka') {
        $item->add_meta_data( __( 'Booking Type', 'chakaride-booking' ), $get_booking_type );
        $item->add_meta_data( __( 'Pickup Location', 'chakaride-booking' ), $get_pickup_location );
        $item->add_meta_data( __( 'Total Distance', 'chakaride-booking' ), $get_distance." KM" );
        $item->add_meta_data( __( 'Pickup Date', 'chakaride-booking' ), $get_pickup_date );
        $item->add_meta_data( __( 'Pickup Time', 'chakaride-booking' ), $get_pickup_time);
        $item->add_meta_data( __( 'Finish Date', 'chakaride-booking' ), $get_dropoff_date );
      }


      if ($get_booking_type == 'daily-basis-outside-dhaka') {
        $item->add_meta_data( __( 'Booking Type', 'chakaride-booking' ), $get_booking_type );
        $item->add_meta_data( __( 'Pickup Location', 'chakaride-booking' ), $get_pickup_location );
        $item->add_meta_data( __( 'Dropoff Location', 'chakaride-booking' ), $get_dropoff_location );
        $item->add_meta_data( __( 'Total Distance', 'chakaride-booking' ), $get_distance." KM" );
        $item->add_meta_data( __( 'Pickup Date', 'chakaride-booking' ), $get_pickup_date );
        $item->add_meta_data( __( 'Pickup Time', 'chakaride-booking' ), $get_pickup_time);
        $item->add_meta_data( __( 'Finish Date', 'chakaride-booking' ), $get_dropoff_date );
      }

    }

    add_action( 'woocommerce_checkout_create_order_line_item', 'chakaride_create_order_items_meta', 10, 4 );



    /**
 * Remove Storefront Breadcrumbs
 */

function chakaride_remove_storefront_breadcrumbs() {
   remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}

add_action( 'init', 'chakaride_remove_storefront_breadcrumbs' );
