<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Pickup extends Widget_Base {


	public function get_name() {
		return 'chakaride-pickup';
	}


	public function get_title() {
		return __( 'Chakaride Pickup', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [ 'elementor-hello-world' ];
	}


	protected function _register_controls() {
		
	}


	protected function render() {
		global $product;
		$settings = $this->get_settings_for_display();

        ?>

        <div id="cr_pickup_container">
        <p>Pickup Location<p>
			<div class="form-group">
				<input type="text" class="form-control" id="cr_pickup" placeholder="Enter Pickup Location">
			</div>
			<button type="button" class="btn btn-primary" id="pickup_button" data-toggle="modal" data-target="#pickup_modal">Search Pickup Location</button>
		</div>
        <!-- <div id="cr_pickup_container"></div> -->

        <?php
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
