<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Trip_Type extends Widget_Base {


	public function get_name() {
		return 'chakaride-trip-type';
	}


	public function get_title() {
		return __( 'Chakaride Trip Type', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [];
	}


	protected function _register_controls() {
		
	}


	protected function render() {
		$settings = $this->get_settings_for_display();

        ?>
        
        <p>Trip Type<p>
        <div class="form-group">
            <!-- <input type="date" class="form-control" id="cr-booking-date" name="cr-booking-date" onchange="set_booking_date()" required> -->
			<select class="custom-select" id="chakaride-trip-type">
            <option selected>Select Trip Type</option>
            <option value="one-way">One Way</option>
            <option value="round-trip">Round Trip</option>
            </select>
			</div>
			</div>
        <!-- <div id="cr_final_location">
        </div> -->
        <?php
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
