<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Booking_Form extends Widget_Base {


	public function get_name() {
		return 'chakaride-booking-form';
	}


	public function get_title() {
		return __( 'Chakaride Booking Form', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [ 'elementor-hello-world' ];
	}


	protected function _register_controls() {

	}


	protected function render() {
		$settings = $this->get_settings_for_display();

        $get_booking_type = $_COOKIE["chakaride-booking-type"];

        // $dropoff_date_label = "Dropoff Date";

        // if ($get_booking_type == 'daily-basis-inside-dhaka' OR $get_booking_type == 'daily-basis-outside-dhaka') {
        //   $dropoff_date_label = "Finish Date";
        // }

        $trip_type_field = '<p>Trip Type<p>
        <div class="form-group">
            <!-- <input type="date" class="form-control" id="cr-booking-date" name="cr-booking-date" onchange="set_booking_date()" required> -->
			<select class="custom-select" id="chakaride-trip-type" <?php echo $trip_type_field_disable ;?>>
            <option selected>Select Trip Type</option>
            <option value="one-way">One Way</option>
            <option value="round-trip">Round Trip</option>
            </select>
			</div>';

      $dropoff_location = '
        <p>Dropoff Location<p>
            <div class="form-group">
            <input type="text" class="form-control" id="cr_dropoff" placeholder="Enter Dropoff Location">
            </div>
        <button type="button" class="btn btn-primary" id="dropoff_button" data-toggle="modal" data-target="#dropoff_modal">Search Dropoff Location</button>
        <div id="cr_dropoff_container"></div><br>
      ';


      $dropoff_time = '
          <p>Dropoff Time<p>
          <div class="form-group">
          <input type="time" class="form-control" id="chakaride-dropoff-time" placeholder="12:30 PM">
      </div>
      ';


      $dropoff_date = '
      <p>Finish Date<p>
        <div class="form-group">
        <input type="date" id="chakaride-dropoff-date" class="form-control" placeholder="dd/mm/yy">
    </div>
    ';

        if ($get_booking_type == 'inside-dhaka-booking' OR $get_booking_type == 'daily-basis-inside-dhaka' OR $get_booking_type == 'daily-basis-outside-dhaka') {
          // $trip_type_field_disable = 'disabled';
          $trip_type_field = '';
        }

        if ($get_booking_type == 'inside-dhaka-booking') {
          // $trip_type_field_disable = 'disabled';
          $dropoff_location = '';
          $dropoff_date = '';
          $dropoff_time = '';
        }


        if ($get_booking_type == 'daily-basis-inside-dhaka') {
          // $trip_type_field_disable = 'disabled';
          $dropoff_location = '';
          $dropoff_time = '';
        }


				if ($get_booking_type == 'daily-basis-outside-dhaka') {
          $dropoff_time = '';
        }

        if ($get_booking_type == 'outside-dhaka-booking') {
          // $trip_type_field_disable = 'disabled';
          $dropoff_date = '';
          $dropoff_time = '';
        }

        if ($get_booking_type == 'airport-booking') {
          $dropoff_date = '';
          $dropoff_time = '';
        }


    //   $dropoff_date = '
    //   <p>'.$dropoff_date_label.'<p>
    //     <div class="form-group">
    //     <input type="date" id="chakaride-dropoff-date" class="form-control" placeholder="dd/mm/yy">
    // </div>
    // ';

        ?>


        <!-- <div class="container" id="chakaride-booking-type-select">
            <div class="row">
                <div class="col-md-3"><button id="inside-dhaka-booking">Inside Dhaka</button></div>
                <div class="col-md-3"><button id="outside-dhaka-booking">Outside Dhaka</button></div>
                <div class="col-md-3"><button id="airport-booking">Airport Booking</button></div>
                <div class="col-md-3"><button id="daily-basis-booking">Daily Basis</button></div>
            </div>
        </div> -->


        <div id="cr-booking-form">

        <div id="cr_pickup_container">
          <p>Pickup Location<p>
			      <div class="form-group">
				      <input type="text" class="form-control" id="cr_pickup" placeholder="Enter Pickup Location">
			      </div>
			    <button type="button" class="btn btn-primary" id="pickup_button" data-toggle="modal" data-target="#pickup_modal">Search Pickup Location</button>
		    </div>

          <!-- <p>Dropoff Location<p>
            <div class="form-group">
				      <input type="text" class="form-control" id="cr_dropoff" placeholder="Enter Dropoff Location">
			      </div>
			    <button type="button" class="btn btn-primary" id="dropoff_button" data-toggle="modal" data-target="#dropoff_modal">Search Dropoff Location</button>
          <div id="cr_dropoff_container"></div><br> -->

          <?php
          echo $dropoff_location;
          echo $trip_type_field ;?>



        <p>Pickup Date<p>
        <div class="form-group">
            <input type="date" id="chakaride-booking-date" class="form-control" placeholder="dd/mm/yy">
			</div>

      <p>Pickup Time<p>
        <div class="form-group">
            <input type="time" class="form-control" id="chakaride-pickup-time" placeholder="12:30 PM">
			</div>

      <?php
      echo $dropoff_date;
      echo $dropoff_time;
      ?>
      <!-- <p>Dropoff Date<p>
        <div class="form-group">
            <input type="date" id="chakaride-dropoff-date" class="form-control" placeholder="dd/mm/yy" <?php echo $dropoff_field_disable;?>>
			</div>

      <p>Dropoff Time<p>
        <div class="form-group">
            <input type="time" class="form-control" id="chakaride-dropoff-time" placeholder="12:30 PM">
			</div> -->
    </div>


        <div class="modal fade" id="pickup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pickup Locations List</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="cr_pickup_modal" class="modal-body">
      </div>
      <div class="mapview" id="pickup_map">
      </div>
      <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="final_distence()">Select Pickup Location</button>
    </div>
  </div>
</div>


<!-- Modal -->

<div class="modal fade" id="dropoff_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Dropoff Locations List</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="cr_dropoff_modal" class="modal-body">
      </div>
      <div class="mapview" id="dropoff_map">
      </div>
      <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="final_distence()">Select Dropoff Location</button>
    </div>
  </div>
</div>
        <!-- <div id="cr_final_location">
        </div> -->
        <?php
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
