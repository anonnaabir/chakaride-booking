<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Booking_Type extends Widget_Base {


	public function get_name() {
		return 'chakaride-booking-type';
	}


	public function get_title() {
		return __( 'Chakaride Booking Type', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [ 'elementor-hello-world' ];
	}


	protected function _register_controls() {
		
	}


	protected function render() {
		$settings = $this->get_settings_for_display();
		$booking_type_selector = '
		<div class="container" id="chakaride-booking-type-select">
            <div class="row d-flex justify-content-center">
                <div ><button class="booking-type-button" id="inside-dhaka-booking">Inside Dhaka<br></button></div>
                <div ><button class="booking-type-button" id="outside-dhaka-booking">Outside Dhaka</button></div>
                <div ><button class="booking-type-button" id="airport-booking">Airport Booking</button></div>
                <div ><button class="booking-type-button" id="daily-basis-booking">Daily Basis</button></div>

				<div ><button class="booking-type-button" id="daily-basis-inside-dhaka">Inside Dhaka (Daily Basis)</button></div>
                <div ><button class="booking-type-button" id="daily-basis-outside-dhaka">Outside Dhaka (Daily Basis)</button></div>
				<div ><button class="booking-type-button" id="booking-selector-back">Go Back</button></div>
            </div> 
			</div>
		';

		
		global $woocommerce;
		if ( WC()->cart->get_cart_contents_count() > 0 ) {
			// echo '
			// <h1>Seems Like You Have An Active Booking. Please check and finish booking or remove the previous booking first.</h1>';
			WC()->cart->empty_cart();
			echo $booking_type_selector;
		}

		else {
			echo $booking_type_selector;
		}
        
        
        
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
