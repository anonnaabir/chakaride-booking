<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Selected_Booking_Type extends Widget_Base {


	public function get_name() {
		return 'chakaride-selected-booking-type';
	}


	public function get_title() {
		return __( 'Chakaride Selected Booking Type', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [ 'elementor-hello-world' ];
	}


	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'plugin-name' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'content_typography',
				'label' => __( 'Typography', 'plugin-domain' ),
				// 'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} #chakaride-selected-booking-type',
			]
		);

		$this->end_controls_section();
	}


	protected function render() {
		$settings = $this->get_settings_for_display();
        $get_booking_type = $_COOKIE["chakaride-booking-type"];
        $booking_type_text = str_replace("-"," ",$get_booking_type );
        echo '<p id="chakaride-selected-booking-type">'.ucwords($booking_type_text).'</p>';
        
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
