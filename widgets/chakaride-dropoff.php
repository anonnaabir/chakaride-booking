<?php
namespace ChakaRide\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ChakaRide_Dropoff extends Widget_Base {


	public function get_name() {
		return 'chakaride-dropoff';
	}


	public function get_title() {
		return __( 'Chakaride Dropoff', 'chakaride-booking' );
	}


	public function get_icon() {
		return 'eicon-posts-ticker';
	}


	public function get_categories() {
		return [ 'general' ];
	}


	public function get_script_depends() {
		return [ 'elementor-hello-world' ];
	}


	protected function _register_controls() {
		
	}


	protected function render() {
		$settings = $this->get_settings_for_display();

        ?>
        
        
        <p>Dropoff Location<p>
        <div class="form-group">
				<input type="text" class="form-control" id="cr_dropoff" placeholder="Enter Dropoff Location">
			</div>
			<button type="button" class="btn btn-primary" id="dropoff_button" data-toggle="modal" data-target="#dropoff_modal">Search Dropoff Location</button>
        <div id="cr_dropoff_container"></div><br>
        <!-- <div id="cr_final_location">
        </div> -->
        <?php
		// echo '<div class="title">';
		// echo $settings['title'];
		// echo '</div>';
	}


}
